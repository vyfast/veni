<!DOCTYPE html>
<html>
<head>
	<title>Home</title>

  <?php include "filehead.php"; ?>

</head>

<body class="grey darken-4">

  <div class="container white background">

   <?php include "header.php"; ?>	 

   <div class="col s12 breadalign">
    <a href="index.php" class="breadcrumb">Pagrindinis</a>
    <a href="infokeliaujantiems.php" class="breadcrumb">Informacija</a>
   </div>   

      <h3 class="center-align">Informacija keliaujantiems</h3>
      <p class="center-align">Čia rasite visą reikalingą informaciją ir nuorodas, kurios padės tinkamai pasiruošti kelionei. Jei turite papildomų klausimų - kreipkitės į VENI atstovus.</p>


   <div class="row">

    <div class="col s12 m12 l12 ">

  <div class="row">
    <div class="col s12">
      <ul class="tabs tabsstyle">
        <li class="tab col s3"><a href="#test1" class="active orange-text">Bendra informacija</a></li>
        <li class="tab col s3"><a class="orange-text" href="#test2">Venera - Svarbu!</a></li>
        <li class="tab col s3"><a href="#test3" class="orange-text text-accent-1">Planuojamos kryptys</a></li>
        <li class="tab col s3"><a href="#test4" class="orange-text">Mokymai</a></li>
      </ul>
    </div>

    <div id="test1" class="col s12">

      <p></p>
      <h6 class="valign-wrapper"><i class="medium material-icons deep-orange-text text-accent-4">announcement</i> Pasirūpinkite galiojančiu transgalaktiniu pasu! Jį įsigyti galite nuorodoje žemiau.</h6>
      <p></p>
         
    <div class="row">
  
  <div class="col s12 m6 l6">
    <div class="card horizontal small">
      <div class="card-image">
        <img src="https://picsum.photos/id/1073/200/200">
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Transgalaktiniai pasai ir kiti kelionių dokumentai</p>
        </div>
        <div class="card-action">
          <a href="https://events.ccc.de/congress/2018/wiki/index.php/Projects:Intergalactic_Passports">Pirkti</a>
        </div>
      </div>
    </div>
    </div>
 
  <div class="col s12 m6 l6">
    <div class="card horizontal small">
      <div class="card-image">
        <img src="https://picsum.photos/id/903/200/200">
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Bendra informacija, žemėlapiai</p>
        </div>
        <div class="card-action">
          <a href="https://www.ted.com/talks/juna_kollmeier_the_most_detailed_map_of_galaxies_black_holes_and_stars_ever_made?utm_source=newsletter_weekly_2019-06-07&utm_campaign=newsletter_weekly&utm_medium=email&utm_content=talk_of_the_week_button">Maps</a>
          <a href="https://voyages.sdss.org/">Info</a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


    <div id="test2" class="col s12">
    <p class="center-align">Kelionėms į Venerą reikalingas specialus fizinis pasiruošimas. Būtina pereiti sveikatos patikrinimo testą. Daugiau informacijos žemiau.</p>

  <div class="row">
      <div class="col s12 m12 l12 center-align">

  <div class="col s12 m6 l6">
    <div class="card horizontal small">
      <div class="card-image">
        <img src="https://picsum.photos/id/953/200/200">
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Praktiniai mokymai Žemėje</p>
        </div>
        <div class="card-action">
          <a href="https://www.youtube.com/watch?v=tGHcyKTIXIM">Daugiau</a>
          <a href="https://www.thoughtco.com/how-astronauts-train-for-space-4153500">Info</a>
        </div>
      </div>
    </div>
    </div>
 
  <div class="col s12 m6 l6">
    <div class="card horizontal small">
      <div class="card-image">
        <img src="https://picsum.photos/id/974/200/200">
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Sveikatos patikrinimas</p>
        </div>
        <div class="card-action">
          <a href="https://www.scientificamerican.com/article/tests-on-astronaut-and-twin-brother-highlight-spaceflights-human-impact/?redirect=1">Testai</a>
          <a href="https://www.nap.edu/read/10903/chapter/1">Info</a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

     </div>

    <div id="test3" class="col s12 center-align">
      
    <div class="col s12 m12 l12 center-align">
        <p>Informacija ruošiama...</p>
        <div class="progress">
        <div class="indeterminate"></div>
        </div>
    </div>
   
    </div>

    <div id="test4" class="col s12">

  <div class="row">
      <div class="col s12 m12 l12 center-align">

  <div class="col s12 m6 l6">
    <div class="card horizontal small">
      <div class="card-image">
        <img src="https://picsum.photos/id/9/200/200">
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>WEB kūrimo mokymai</p>
        </div>
        <div class="card-action">
          <a href="https://www.vilniuscoding.lt/mokymai/web-kurimo-mokymai-vakariniai-vilniuje/">Daugiau</a>
        </div>
      </div>
    </div>
    </div>
 
  <div class="col s12 m6 l6">
    <div class="card horizontal small">
      <div class="card-image">
        <img src="https://picsum.photos/id/903/200/200">
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Praktiniai mokymai ir testai</p>
        </div>
        <div class="card-action">
          <a href="http://mdrs.marssociety.org/apply-to-field-season/">Info</a>
          <a href="http://play.quantumgame.io/">Tests</a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>

</div>
</div>
</div>

  <?php include "footer.php"; ?>

  <?php include "filebottom.php"; ?>


</div>

</body>
</html>