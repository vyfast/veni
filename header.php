<div class="grey darken-4">

	<!-- Navbar goes here -->

	<!-- Dropdown Structure -->
	<ul id="dropdown1" class="dropdown-content">
		<li><a href="menulis.php" class="grey-text text-darken-4">Mėnulis</a></li>
		<li><a href="marsas.php" class="grey-text text-darken-4">Marsas</a></li>

		<li><a href="venera.php" class="grey-text text-darken-4">Venera</a></li>

		<li class="divider"></li>
		<li><a href="infokeliaujantiems.php" class="grey-text text-darken-4">INFO</a></li>
	</ul>
	<nav class="grey darken-4 hoverable">
		<div class="nav-wrapper">
			<a href="index.php" class="brand-logo logo"><img class="logo" src="images/spaceFly.png"></a>


			<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
			<ul class="right hide-on-med-and-down">
				<li><a href="index.php">Pagrindinis</a></li>
				<li><a href="about.php">Apie mus</a></li>
				<!-- Dropdown Trigger -->
				<li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Kelionės<i class="material-icons right">arrow_drop_down</i></a></li>
			</ul>
		</div>
	</nav>

	<ul class="sidenav" id="mobile-demo">
		<li><a href="index.php">Pagrindinis</a></li>
		<li><a href="index.php">Kelionės</a></li>
		<li><a href="infokeliaujantiems.php">INFO</a></li>	    
		<li><a href="about.php">Apie mus</a></li>
	</ul>



</div>