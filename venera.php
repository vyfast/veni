<!DOCTYPE html>
<html>
<head>
	<title>Venera main</title>

  <?php include "filehead.php"; ?>

</head>

<body class="grey darken-4">

  <div class="container white background">

    <?php include "header.php"; ?>	
    <div class="col s12 breadalign">
    <a href="index.php" class="breadcrumb">Pagrindinis</a>
    <a href="venera.php" class="breadcrumb">Venera</a>
    
  </div>

    <h3 class="center-align" id="planetname">Venera</h3>

    <div class="row">

      <div class="col s12 m12 l12 ">

        <p class="center-align">Jus traukia ekstremalios sąlygos ir neįprastos veiklos? Oro uostų detektoriai fiksuoja geležinius nervus? Tuomet kelionės į Venerą negalite praleisti. Mūsų naujausia kelionių kryptis tikrai pakels adrenalino lygį! Čia visada karšta ir viskas kitaip. Kokioje dar iš Žemės pasiekiamoje planetoje susidursite su reiškiniu kai diena yra ilgesnė už metus, o Saulė leidžiasi rytuose? Arba galėsite stebėti sieros lietų? Registruokitės į įsimintiniausią gyvenimo kelionę! Vietų kiekis ribotas. Yra papildomų sąlygų.</p>

      </div>  
      </div>

      <?php include "databaseveni.php"; ?>
  
  <div class="row flex">

      <div class="col s12 m6 l6">
       <div class="card">
        <div class="card-image ">
          <img class="jpg" src="images/venera1.jpg">
          <a href=https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwi_o6XU99fiAhWKDuwKHd7-BhsQMwgqKAAwAA&url=https%3A%2F%2Fsolarsystem.nasa.gov%2Fplanets%2Fvenus%2Fgalleries%2F&psig=AOvVaw2E_2ZV9YGm5Lfs_Bu5rL5B&ust=1560016283527614&ictx=3&uact=3 class="m"></a> 
          <span class="card-title">Venera galerija</span>
        </div>
        <div class="card-content">
          <p> Kelionių datos ir programos
          </p>
        </div>
        <div class="card-action">
          <a href="laikas3.php">Pasirinkti</a>
        </div>
      </div>
    </div>

  <div class="col s12 m6 l6">
    <h5 class="center-align"> Registracija į kelionę</h5>
  <form class="col s12 m12 l12">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="first_name" type="text" class="validate" name="name">
          <label for="first_name">Vardas</label>
        </div>
        <div class="input-field col s12 m12 l12">
          <input id="last_name" type="text" class="validate" name="surname">
          <label for="last_name">Pavardė</label>
        </div>
          <div class="input-field col s12 m12 l12">
          <input id="email" type="text" class="validate" name="email">
          <label for="email">El.paštas</label>
        </div>
          <div class="input-field col s12 m12 l12">
          <input id="date" type="text" class="datepicker" name="journey">
          <label for="date">Pageidaujama kelionės data</label>
        </div>
      </div>
      <div class="input-field col s12 m12 l12 hide">
       <input id="planetnameinput" type="text" name="planet" value="">
       </div>
        <div class="center-align">
          <button class="btn waves-effect waves-light orange accent-4">Siųsti</button>
        </div>
    </form>
  </div>

  </div>

    <?php include "footer.php"; ?>

    <?php include "filebottom.php"; ?>


  </div>

</body>
</html>