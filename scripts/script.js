    $(document).ready(function(){
    $('.dropdown-trigger').dropdown();  
    $('.sidenav').sidenav();
    $('.tabs').tabs();
    $('.modal').modal();
    $('.carousel').carousel();
    $('.datepicker').datepicker();
    var planet = $('#planetname').text();
    $('#planetnameinput').attr('value', planet);

    $('.tabsstyle.tabs .tab a').on('click', function() {
    	var nuoroda = $(this).attr('href');
    	console.log(nuoroda);
    	window.location.hash = nuoroda;
    });

 });
 