 <footer class="page-footer grey darken-4">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="white-text">VENI</h5>
        <p class="white-text">Poilsinės, egzotinės, pažintinės kelionės, kruizai, paskutinė minutė.</p>
      </div>
      <div class="col l4 offset-l2 s12">
        <h5 class="white-text">Mūsų partneriai</h5>
        <ul>
          <li><a class="grey-text text-darken-2" href="https://www.nasa.gov/">NASA</a></li>
          <li><a class="grey-text text-darken-2" href="https://www.spacex.com/">SPACEX</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright grey darken-3 white-text">
    <div class="container grey darken-3">
      <p>© 2019 Copyright Text
      <a class="white-text right" href="https://www.youtube.com/watch?v=sX1Y2JMK6g8">Tesla in space</a></p>
    </div>
  </div>
</footer>