<!DOCTYPE html>
<html>
<head>
	<title>Venera laikas3</title>
	<?php include "filehead.php"; ?>


</head>

	<body class="grey darken-4">

  <div class="container white background">

	<?php include "header.php"; ?>
  <div class="col s12 breadalign">
    <a href="index.php" class="breadcrumb">Pagrindinis</a>
    <a href="venera.php" class="breadcrumb">Venera</a>
    <a href="laikas3.php" class="breadcrumb">Kelionės į Venerą</a>
  </div>

	 <h3 class="center-align">Kelionės į Venerą</h3>


 <div class="row">

       <div class="col s12 m12 l12 center-align">
        <ul class="collection with-header">
        <li class="collection-header"><h4>Datos</h4></li>
        <li class="collection-item">2020-09-15 - 2030-09-15</li>
        <li class="collection-item">2025-06-01 - 2035-07-01</li>
        <li class="collection-item">2030-01-01 - 2040-02-01</li>
        <li class="collection-item">2045-11-01 - 2055-12-01</li>
      </ul>
      </div>

      <h4 class="center-align">Programos ir renginiai</h4>  

    <div class="row">
      <div class="col s12 m12 l12">
    <div class="col s12 m4 l4">
      <div class="card large">
        <div class="card-image">
          <img src="images/venus-festival2.jpeg" class="journeycard">
          <span class="card-title">Poilsis Veneroje</span>
  <!--         <a href="kainininkas.php" class="btn-floating halfway-fab waves-effect waves-light orange accent-4"><i class="material-icons">add</i></a> -->
        </div>
        <div class="card-content">
          <p>SEZONO PASIŪLYMAS. Festivalis "Burned Man" - avangardinio meno ir naujausių technologijų sintezė. Bilietai prekyboje netrukus! Registracija dalyviams ir instaliacijų anketos <a href="https://burningman.org/event/brc/2019-art-installations/">čia.</a></p>
        </div>
        <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
      </div>
    </div>
 
     <div class="col s12 m4 l4">
      <div class="card large">
        <div class="card-image">
          <img src="https://picsum.photos/id/548/300/200" class="journeycard">
          <span class="card-title">Poilsis Veneroje</span>
 <!--          <a href="kainininkas.php" class="btn-floating halfway-fab waves-effect waves-light orange accent-4"><i class="material-icons">add</i></a> -->
        </div>
        <div class="card-content">
          <p>Vulkanų išsiveržimo stebėjimas.</p>
        </div>
        <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
      </div>
    </div>

<!--     <div class="col s12 m4 l4">
      <div class="card">
        <div class="card-image">
          <img src="https://picsum.photos/id/618/300/200" class="journeycard">
          <span class="card-title">Poilsis Veneroje</span>
          <a href="kainininkas.php" class="btn-floating halfway-fab waves-effect waves-light orange accent-4"><i class="material-icons">add</i></a>
        </div>
        <div class="card-content">
          <p>Ekskursija su gidu į interaktyvų cheminių eksperimentų mokslo centrą.</p>
        </div>
      </div>
    </div> -->
 
    <div class="col s12 m4 l4">
      <div class="card large">
        <div class="card-image">
          <img src="https://picsum.photos/id/680/300/200" class="journeycard">
          <span class="card-title">Poilsis Veneroje</span>
  <!--         <a href="kainininkas.php" class="btn-floating halfway-fab waves-effect waves-light orange accent-4"><i class="material-icons">add</i></a> -->
        </div>
        <div class="card-content">
          <p>Aplankykite ekstramalų karštąjį SPA centrą "Lucifer".</p>
        </div>
        <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
      </div>
    </div>
  </div>
 </div>



  <?php include "footer.php"; ?>

  <?php include "filebottom.php"; ?>


</div>
	

</body>
</html>