<?php
 
$dataPoints = array(
	array("label"=> "Maistas + Gerimai", "y"=> 590),
	array("label"=> "Veikla ir pramogos", "y"=> 261),
	array("label"=> "Sveikata ir fitnesas", "y"=> 158),
	array("label"=> "Pirkiniai ir kitos prekės", "y"=> 172),
	array("label"=> "Transportas", "y"=> 191),
	array("label"=> "Nuoma", "y"=> 573),
	array("label"=> "Keliones draudimas", "y"=> 250)
);
	
?>
<!DOCTYPE HTML>
<html>
<head>  
<script>
window.onload = function () {
 
var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	exportEnabled: true,
	title:{
		text: "Vidutiniskos islaidos dienai JAV Doleriai"
	},
	subtitles: [{
		text: "Naudojama valiuta: JAV Doleriai ($)"
	}],
	data: [{
		type: "pie",
		showInLegend: "true",
		legendText: "{label}",
		indexLabelFontSize: 16,
		indexLabel: "{label} - #percent%",
		yValueFormatString: "฿#,##0",
		dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();
 
}
</script>
</head>
<body>
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>             