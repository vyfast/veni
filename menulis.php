<!DOCTYPE html>
<html>
<head>
	<title>Mėnulis main</title>

  <?php include "filehead.php"; ?>

</head>

<body class="grey darken-4">

  <div class="container white background">

    <?php include "header.php"; ?>	

    <div class="col s12 breadalign">
    <a href="index.php" class="breadcrumb">Pagrindinis</a>
    <a href="menulis.php" class="breadcrumb">Mėnulis</a>
    </div>

    <h3 class="center-align" id="planetname">Mėnulis</h3>


    <div class="row">

      <div class="col s12 m12 l12 ">

        <p class="center-align">Mėnulis - dangaus kūnas, Žemės palydovas. Daugybę metų žmonės ruošėsi kelionei į Mėnulį. Daugybę metų žmonės pasikliovė Mėnuliu kaip žibintu naktį. Daugybę metų Mėnulis buvo ir yra menininkų, dailininkų, kūrėjų įkvėpimo šaltinis. Daugybę kartų žmonės į jį žiūrėjo ir mąstė appie įvairias gyvenimo tiesas, apie rūpesčius, apie džiaugsmus.
        </p>

        <p class="center-align">Iki šiol nesilankėte Mėnulyje? Pasinaudokite proga ir susiplanuokite atostogas į populiariausią kosminį kurortą. Pasiimkite visą šeimą - puikiai išvystyta infrastruktūra, įvairios kultūrinės programos ir didelis "viskas įskaičiuota" variantų pasirinkimas leis pailsėti nuo kasdienių žemiškųjų rūpesčių. Ramūs pasivaikščiojimai jūromis ir krateriais, romantiškas saulėlydžio stebėjimas su Žeme horizonte ar įsimintina dviejų dienų ekskursija į tamsiąją Mėnulio pusę - viskas ko reikia idealioms atostogoms.</p>

     </div>
   </div>

        <?php include "databaseveni.php"; ?>

  <div class="row flex">

  <div class="col s12 m6 l6">
      <div class="card">
        <div class="card-image">
          <img class="jpg" src="images/moon.jpg">
           <a href="https://www.google.com/search?sa=X&biw=1904&bih=952&q=moon+images+hd&tbm=isch&source=univ&ved=2ahUKEwjLu5DF7dfiAhVBJFAKHRZSDggQsAR6BAgHEAE" class="m"></a> 
          <span class="card-title">Mėnulis galerija</span>
          </div>
        <div class="card-content">
          <p> Kelionių datos ir programos
          </p>
        </div>
        <div class="card-action">
          <a href="laikas1.php">Pasirinkti</a>
        </div>
      </div>
    </div>
  
 <div class="col s12 m6 l6">
    <h5 class="center-align"> Registracija į kelionę</h5>
   <form class="col s12 m12 l12">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="first_name" type="text" class="validate" name="name">
          <label for="first_name">Vardas</label>
        </div>
        <div class="input-field col s12 m12 l12">
          <input id="last_name" type="text" class="validate" name="surname">
          <label for="last_name">Pavardė</label>
        </div>
          <div class="input-field col s12 m12 l12">
          <input id="email" type="text" class="validate" name="email">
          <label for="email">El.paštas</label>
        </div>
         <div class="input-field col s12 m12 l12">
          <input id="date" type="text" class="datepicker" name="journey">
          <label for="date">Pageidaujama kelionės data</label>
        </div>
      </div>
      <div class="input-field col s12 m12 l12 hide">
          <input id="planetnameinput" type="text" name="planet" value="">
         </div>
        <div class="center-align">
            <button class="btn waves-effect waves-light orange accent-4">Siųsti</button>
        </div>
      </form>
   </div>

  </div>


    <?php include "footer.php"; ?>

    <?php include "filebottom.php"; ?>


  </div>
 
</body>
</html>