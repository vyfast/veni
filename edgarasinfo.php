<!DOCTYPE html>
<html>
<head>
	<title>Home</title>

  <?php include "filehead.php"; ?>

</head>

<body class="grey darken-4">

  <div class="container white background">

   <?php include "header.php"; ?>	    
   <div class="col s12 breadalign">
    <a href="index.php" class="breadcrumb">Pagrindinis</a>
    <a href="about.php" class="breadcrumb">Apie</a>
    <a href="edgarasinfo.php" class="breadcrumb">Edgaras</a>
  </div>
  <h3 class="center-align">Edgaras Han Solo</h3>
  <h6 class="grey-text text-darken-2 center-align quote">Moto</h6>
  
  <div class="row">

    <div class="col s12 m12 l12 ">

      <p class="center-align">Savo įspūdžius Gizos slėnyje galėčiau prilyginti nebent skrydžiui į Mėnulį, nes, manding, astronautai jautė maždaug tą patį, kai žengė pirmuosius žingsnius dangaus kūnu, kurį nuo pat vaikystės matė dešimtis tūkstančių kartų. Tegul neįsižeis astronautai, bet aš nė kiek neperdedu.</p>

      <div class="row center-align">
        <div class="col s12 m12 l12">
          <img src="https://picsum.photos/id/904/250/150" class="card personimg">
          <img src="https://picsum.photos/id/883/250/150" class="card personimg">
          <img src="https://picsum.photos/id/868/250/150" class="card personimg">

        </div>
      </div>

    </div>

  </div>


  <?php include "footer.php"; ?>

  <?php include "filebottom.php"; ?>


</div>

</body>
</html>