<!DOCTYPE html>
<html>
<head>
	<title>Menulis laikas1</title>
	<?php include "filehead.php"; ?>


</head>

	<body class="grey darken-4">

  <div class="container white background">

	<?php include "header.php"; ?>

  <div class="col s12 breadalign">
    <a href="index.php" class="breadcrumb">Pagrindinis</a>
    <a href="menulis.php" class="breadcrumb">Mėnulis</a>
    <a href="laikas1.php" class="breadcrumb">Kelionės į Mėnulį</a>
  </div>

	<h3 class="center-align">Kelionės į Mėnulį</h3>

 <div class="row">

       <div class="col s12 m12 l12 center-align">
        <ul class="collection with-header">
        <li class="collection-header"><h4>Datos</h4></li>
        <li class="collection-item">2020-09-15 - 2030-09-15</li>
        <li class="collection-item">2025-06-01 - 2035-07-01</li>
        <li class="collection-item">2030-01-01 - 2040-02-01</li>
        <li class="collection-item">2045-11-01 - 2055-12-01</li>
      </ul>
      </div>

      <h4 class="center-align">Programos ir renginiai</h4>  

  <div class="row">
    <div class="col s12 m12 l12">

    <div class="col s12 m4 l4">
      <div class="card large">
        <div class="card-image">
          <img src="https://picsum.photos/id/515/300/200" class="journeycard">
          <span class="card-title">Poilsis Menulyje</span>
          </div>
          <div class="card-content">
          <p>2 dienų ekskursija į Tamsiąją Mėnulio pusę visai šeimai. Įranga ir maitinimas įskaičiuoti.</p>
        </div>
        <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
  </div>
</div>

    <div class="col s12 m4 l4">
      <div class="card large">
        <div class="card-image">
          <img src="https://picsum.photos/id/156/300/200" class="journeycard">
          <span class="card-title">Poilsis Menulyje</span>
        </div>
        <div class="card-content">
          <p>Aplankykite archeologijos parodą - "Pirmieji Žmonės Mėnulyje".</p>
        </div>
        <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
      </div>
    </div>
 
    <div class="col s12 m4 l4">
      <div class="card large">
        <div class="card-image">
          <img src="images/moon-weight_less.jpg" class="journeycard">
          <span class="card-title">Poilsis Menulyje</span>
        </div>
        <div class="card-content">
          <p>Psichologinio sveikatinimo stovykla antsvorio turintiems žmonėms - Mėnulyje žmogus sveria net 6x mažiau nei Žemėje. Atsikratykite kompleksų iškart!</p>
        </div>
        <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
      </div>
    </div>
  </div>
</div>



  <?php include "footer.php"; ?>

  <?php include "filebottom.php"; ?>


</div>
	

</body>
</html>