<!DOCTYPE html>
<html>
<head>
	<title>Marsas laikas2</title>
	<?php include "filehead.php"; ?>


</head>

	<body class="grey darken-4">

  <div class="container white background">

	<?php include "header.php"; ?>
  <div class="col s12 breadalign">
    <a href="index.php" class="breadcrumb">Pagrindinis</a>
    <a href="marsas.php" class="breadcrumb">Marsas</a>
    <a href="laikas2.php" class="breadcrumb">Kelionės į Marsą</a>
  </div>


  <h3 class="center-align">Kelionės į Marsą</h3>

 <div class="row">

       <div class="col s12 m12 l12 center-align">
        <ul class="collection with-header">
        <li class="collection-header"><h4>Datos</h4></li>
        <li class="collection-item">2020-09-15 - 2030-09-15</li>
        <li class="collection-item">2025-06-01 - 2035-07-01</li>
        <li class="collection-item">2030-01-01 - 2040-02-01</li>
        <li class="collection-item">2045-11-01 - 2055-12-01</li>
      </ul>
      </div>

      <h4 class="center-align">Programos ir renginiai</h4>  

    <div class="row">
      <div class="col s12 m12 l12">
    <div class="col s12 m4 l4">
      <div class="card medium">
        <div class="card-image">
          <img src="https://picsum.photos/id/525/300/200" class="journeycard">
          <span class="card-title">Poilsis Marse</span>
          </div>
        <div class="card-content">
          <p>Civilizacijos paieškos - kultūrinis žygis Marso kanjonais. Tinka ir pradedantiesiems.</p>
        </div>
        <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
      </div>
    </div>

      
     <div class="col s12 m4 l4">
      <div class="card medium">
        <div class="card-image">
          <img src="images/mars-ride1.jpeg" class="journeycard">
          <span class="card-title">Poilsis Marse</span>
          </div>
        <div class="card-content">
          <p>Pasivažinėjimas marsaeigiais.</p>
        </div>
        <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
      </div>
    </div>
 
       <div class="col s12 m4 l4">
      <div class="card medium">
        <div class="card-image">
          <img src="images/mars-hiking1.jpeg" class="journeycard">
          <span class="card-title">Poilsis Marse</span>
        </div>
        <div class="card-content">
          <p>Ekstremalus vulkanų žygis.</p>
        </div>
       <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
      </div>
    </div>
  </div>
</div>


    <div class="row">
      <div class="col s12 m12 l12">
    <div class="col s12 m4 l4">
      <div class="card medium">
        <div class="card-image">
          <img src="images/mars-acroyoga.jpg" class="journeycard">
          <span class="card-title">Poilsis Marse</span>
        </div>
        <div class="card-content">
          <p>5 dienų akrojogos stovykla Marso palydove Fobe.</p>
        </div>
        <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
      </div>
    </div>
  
      <div class="col s12 m4 l4">
      <div class="card medium">
        <div class="card-image">
          <img src="images/mars-marathon1.jpg" class="journeycard">
          <span class="card-title">Poilsis Marse</span>
        </div>
        <div class="card-content">
          <p><h6 class="red-text">NAUJIENA</h6> Marso žiemos maratonas. Liko tik kelios vietos!</p>
        </div>
        <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
      </div>
    </div>

    <div class="col s12 m4 l4">
      <div class="card medium">
        <div class="card-image">
          <img src="images/martian-chronicles.jpg" class="journeycard">
          <span class="card-title">Poilsis Marse</span>
           </div>
        <div class="card-content">
          <p>Tarpplanetinis fantastinių filmų festivalis "Marso kronikos".</p>
        </div>
        <div class="card-action">
        <a href="kainininkas.php">Kainos</a>
    </div>
      </div>
    </div>
  </div>
 </div>



  <?php include "footer.php"; ?>

  <?php include "filebottom.php"; ?>


</div>
	

</body>
</html>