<!DOCTYPE html>
<html>
<head>

	<title>Keliones kainos</title>

	<?php include "filehead.php"; ?>

</head>



<body class="grey darken-4">

	<div class="container white background">

		<?php include "header.php"; ?>

	<div class="col s12 breadalign">
    <a href="index.php" class="breadcrumb">Pagrindinis</a>
    <a href="index.php"class="breadcrumb">Kelionės</a>
    <a href="kainininkas.php" class="breadcrumb">Kainininkas</a>
    </div>

	<?php
 
$dataPoints = array(
	array("label"=> "Maistas ir gėrimai", "y"=> 590),
	array("label"=> "Pramogos", "y"=> 261),
	array("label"=> "Sveikata ir fitnesas", "y"=> 158),
	array("label"=> "Pirkiniai ir kitos išlaidos", "y"=> 172),
	array("label"=> "Transportas", "y"=> 191),
	array("label"=> "Apgyvendinimas", "y"=> 573),
	array("label"=> "Kelionės draudimas", "y"=> 250)
);
	
?>
  
<script>
window.onload = function () {
 
var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	exportEnabled: false,
	title:{
		text: "Vidutinės kelionės išlaidos dienai"
	},
	subtitles: [{
		text: "Naudojama valiuta: Bitcoin (₿)"
	}],
	data: [{
		type: "pie",
		showInLegend: "true",
		legendText: "{label}",
		indexLabelFontSize: 16,
		indexLabel: "{label} - #percent%",
		yValueFormatString: "₿#,##0",
		dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();
 
}

</script>

<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<?php include "footer.php"; ?>

  <?php include "filebottom.php"; ?>

</div>            

</body>
</html>