<!DOCTYPE html>
<html>
<head>
  <title>Apie Mus</title>

  <?php include "filehead.php"; ?>
  <style>
    @import url('https://fonts.googleapis.com/css?family=Orbitron&display=swap');
  </style>

</head>

<body class="grey darken-4">


  <div class="container white background">

   <?php include "header.php"; ?>	   

   <div class="nav-wrapper">
    <div class="col s12 breadalign">
      <a href="index.php" class="breadcrumb">Pagrindinis</a>
      <a href="about.php" class="breadcrumb">Apie</a>
    </div>

  </div>
  <h3 class="center-align">Apie mus</h3>
 
  <div class="row">

   <div class="col s12 m12 l12">
    <div class="col s12 m12 l12">
      <span class="flow-text">
        <p class="infotext center-align">Organizuojame poilsines, turistines bei ekstremalias keliones į įvairias Saulės sistemos planetas ir planetų mėnulius.</p>
      </span>
    </div>
  </div>

  <h4 class="center-align">Mūsų gidų komanda</h4>

<!-- Carousel cia -->

<div class="carousel">
 <a class="carousel-item" href="vytasinfo.php">

  <div class="card">
    <div class="card-image">
      <img src="https://picsum.photos/id/669/400/400">
      <span class="card-title center-align">Vytautas Skywalker</span>
    </div>
  </div>
</a>
<a class="carousel-item" href="nerijusinfo.php">
  <div class="card">
    <div class="card-image">
      <img src="https://picsum.photos/id/338/400/400">
      <span class="card-title center-align">Nerijus Obi-Wan Kenobi</span>
    </div>
  </div>
</a>
<a class="carousel-item" href="indreinfo.php">
  <div class="card">
    <div class="card-image">
      <img src="https://picsum.photos/id/550/400/400">
      <span class="card-title center-align">Indrė Mara Jade</span>
    </div>
  </div>
</a>
<a class="carousel-item" href="edgarasinfo.php">
  <div class="card">
    <div class="card-image">
      <img src="https://picsum.photos/id/177/400/400">
      <span class="card-title center-align">Edgaras Han Solo</span>
    </div>
  </div>
</a>
</div>

<!-- Carousel baigiasi -->

</div>

<!-- Map'as ir rekvizitai cia -->

<div class="row flex">

 <div class="col s12 l7 center-align">
  <ul class="collection with-header">
    <li class="collection-header"><h4>Kaip mus rasti</h4></li>
    <li class="collection-item">
     <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="350" id="gmap_canvas" src="https://maps.google.com/maps?q=Ulonu%20g.%205%2C%20vilnius%2C%20lithuania&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.pureblack.de"></a></div><style>.mapouter{position:relative;text-align:right;height:350px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:350px;width:600px;}</style>Web design by Pure Black. Inquiries: info@pureblack.de</div>
   </li>
 </ul>
</div>

<div class="col s12 l5 center-align">
  <ul class="collection with-header">
    <li class="collection-header"><h4>Rekvizitai</h4></li>
    <li class="collection-item"><p>Ulonų g. 5, I laiptinė, 3 aukštas, 313 kab., Vilnius, Lietuva</p></li>
    <li class="collection-item">
      <p>+370 112233000</p>
      <p>+370 332211000</p>
      <p>+370 223311000</p>
    </li>
    <li class="collection-item"><p>info@venitravel.lt</p></li>
    <li class="collection-item">
      <div class="row flex socialmedia-img">
         <div class="col s4 m4 l4 center-align">
          <a href="https://www.facebook.com/spacecom/"><img src="images/facebook-icon-o.png" class="circle responsive-img"></a></div>
          <div class="col s4 m4 l4 center-align">
            <a href="https://twitter.com/SPACEdotcom?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor"><img src="images/twitter-icon-o.png" class="circle responsive-img"></a></div>
            <div class="col s4 m4 l4 center-align">
              <a href="https://www.instagram.com/spacedotcom/?hl=en"><img src="images/instagram-icon-o.png" class="circle responsive-img"></a></div>
             </div>
        </li>
      </ul>
    </div>

  </div>

  <!-- Footer cia -->

  <?php include "footer.php"; ?>

  <?php include "filebottom.php"; ?>


</div>

</body>
</html>