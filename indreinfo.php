<!DOCTYPE html>
<html>
<head>
	<title>Home</title>

<?php include "filehead.php"; ?>

</head>

<body class="grey darken-4">

  <div class="container white background">

	<?php include "header.php"; ?>	
  <div class="col s12 breadalign">
    <a href="index.php" class="breadcrumb">Pagrindinis</a>
    <a href="about.php" class="breadcrumb">Apie</a>
    <a href="indreinfo.php" class="breadcrumb">Indrutė</a>
  </div>    

    <h3 class="center-align">Indrė Mara Jade</h3>
    <h6 class="grey-text text-darken-2 center-align quote">Moto</h6>
  
    <div class="row">

      <div class="col s12 m12 l12 ">

        <p class="center-align">Kiekvienas žmogus - tai paskira planeta, kontinentas, šalis arba miestas. Bendravimas su kitais prilygsta kelionėms ir jų būna visokių: pradedant įprastomis draugystės išvykomis, baigiant meilės kryžiaus žygiais.</p>

        <div class="row center-align">
        <div class="col s12 m12 l12">
        <img src="https://picsum.photos/id/1048/250/150" class="card personimg">
        <img src="https://picsum.photos/id/1006/250/150" class="card personimg">
        <img src="https://picsum.photos/id/326/250/150" class="card personimg">
        </div>
        </div>

      </div>

    </div>


  <?php include "footer.php"; ?>

  <?php include "filebottom.php"; ?>


</div>

</body>
</html>