<!DOCTYPE html>
<html>
<head>
	<title>Home</title>

  <?php include "filehead.php"; ?>

</head>

<body class="grey darken-4">

  <div class="container white background">

    <?php include "header.php"; ?>	

    <h3 class="center-align">VENI - poilsis kitoje planetoje</h3>
    <h6 class="grey-text text-darken-2 center-align quote">Jokie vaistai negydo taip kaip kosminės kelionės</h6>    

    <div class="row">

      <div class="col s12 m12 l12">

        <p class="center-align">Organizuojame poilsines, turistines bei ekstremalias keliones į įvairias Saulės sistemos planetas ir planetų mėnulius. Artimiausiu metu planuojame organizuoti temines keliones į Centauri Septum galaktiką ir Tatuine planetą.</p>

      </div>

    </div>
    <div class="planetSelect">
      <img class="planets1" src="images/planets_image.jpg">
      <a href="marsas.php" class="marsBut"></a> 
      <a href="menulis.php" class="moonBut"></a>
      <a href="venera.php" class="venusBut" ></a>
      
      <!-- mercury -->
      <a href="#" class="mercuryBut modal-trigger" data-target='mercury1'></a>
      <div id="mercury1" class="modal planet">
        <div class="modal-content">
          <h4>MERKURIJUS</h4>
          <p>Pirmoji Saulės sistemos planeta.</p>
          <p>Kelionės bus organizuojamos artimiausiu laiku,</p>
          <p>Sekite naujienas.</p>
          <div class="mercury2" >
            <img src="images/mercury.jpg" style="width:21rem;">
          </div>
        </div>
      </div>
      <!-- mercury -->

      <!-- jupiter -->
      <a href="#" class="jupiterBut modal-trigger" data-target='jupiter1'></a>
      <div id="jupiter1" class="modal planet">
        <div class="modal-content">
          <h4>JUPITERIS</h4>
          <p>Penktoji Saulės sistemos planeta.</p>
          <p>Kelionės bus organizuojamos artimiausiu laiku,</p>
          <p>Sekite naujienas.</p>
          <div class="jupiter2" >
            <img src="images/jupiter.png" style="width:21rem;">
          </div>
        </div>
      </div>
      <!-- jupiter -->

      <!-- saturn -->
      <a href="#" class="saturnBut modal-trigger" data-target='saturn1'></a>
      <div id="saturn1" class="modal planet">
        <div class="modal-content">
          <h4>SATURNAS</h4>
          <p>Šeštoji Saulės sistemos planeta.</p>
          <p>Kelionės bus organizuojamos artimiausiu laiku,</p>
          <p>Sekite naujienas.</p>
          <div class="saturn2" >
            <img src="images/saturn.jpg" style="width:21rem;">
          </div>
        </div>
      </div>
      <!-- saturn -->

      <!-- uranus -->
      <a href="#" class="uranusBut modal-trigger" data-target='uranus1'></a>
      <div id="uranus1" class="modal planet">
        <div class="modal-content">
          <h4>URANAS</h4>
          <p>Septintoji Saulės sistemos planeta.</p>
          <p>Kelionės bus organizuojamos artimiausiu laiku,</p>
          <p>Sekite naujienas.</p>
          <div class="uranus2" >
            <img src="images/uranus.jpg" style="width:21rem;">
          </div>
        </div>
      </div>
      <!-- uranus -->

      <!-- neptune -->
      <a href="#" class="neptuneBut modal-trigger" data-target='neptune1'></a>
      <div id="neptune1" class="modal planet">
        <div class="modal-content">
          <h4>NEPTŪNAS</h4>
          <p>Aštuntoji Saulės sistemos planeta.</p>
          <p>Kelionės bus organizuojamos artimiausiu laiku,</p>
          <p>Sekite naujienas.</p>
          <div class="neptune2" >
            <img src="images/neptune.jpg" style="width:21rem;">
          </div>
        </div>
      </div>
      <!-- neprune -->

      <!-- pluto -->
      <a href="#" class="plutoBut modal-trigger" data-target='pluto1'></a>
      <div id="pluto1" class="modal planet">
        <div class="modal-content">
          <h4>PLUTONAS</h4>
          <p>Devintoji Saulės sistemos planeta.</p>
          <p>Kelionės bus organizuojamos artimiausiu laiku,</p>
          <p>Sekite naujienas.</p>
          <div class="pluto2" >
            <img src="images/pluto.jpg" style="width:21rem;">
          </div>
        </div>
      </div>
      <!-- pluto -->

    </div>

    <h4 class="center-align">Populiariausios kelionės</h4>

    
    <div class="row unikalu">

      <!-- mars -->
      <div class="row" style="width:33%; min-width: 300px; left:20px; display: inline-block;">
        <div class="col s4 m4 l4" style="width:100%;">
          <div class="card medium">
            <div class="card-image">
              <img src="images/vytauto/dims.jfif" class="journeycard">
              <span class="card-title">MARSAS</span>
            </div>
            <div class="card-content">
              <p>Aktyvus poilsis Marse. Tikra atgaiva kūnui nuo intensyvaus protinio darbo!</p>
            </div>

            <a href="marsas.php" class="quickTripBut"></a>

          </div>
        </div>
      </div>

      <!-- moon -->
      <div class="row" style="width:33%; min-width: 300px; left:20px; display: inline-block;">
        <div class="col s4 m4 l4" style="width:100%;">
          <div class="card medium">
            <div class="card-image">
              <img src="images/vytauto/chill.jpg" class="journeycard">
              <span class="card-title">MĖNULIS</span>
            </div>
            <div class="card-content">
              <p>Norite pasijausti lengvesni? Atlikti super šuolius į tolį? Prabangūs 8* viešbučiai su nuostabiu vaizdu į Žaliąją planetą. Mėnulis - poilsis kurio nepamiršite!</p>
            </div>

            <a href="menulis.php" class="quickTripBut"></a>

          </div>
        </div>
      </div>

      <!-- venus -->
      <div class="row"style="width:33%; min-width: 300px; left:20px; display: inline-block;">
        <div class="col s12 m4 l4" style="width:100%;">
          <div class="card medium">
            <div class="card-image">
              <img src="images/vytauto/tatooine-ft.jpg" class="journeycard">
              <span class="card-title">VENERA</span>
            </div>
            <div class="card-content">
              <p>Patinka kopti į ugnikalnius? Ar matėte Saulę iš arti? Venera - nuostabi ir karšta planeta!</p>
            </div>

            <a href="venera.php" class="quickTripBut"></a>

          </div>
        </div>
      </div> 
    </div>


    <?php include "footer.php"; ?>

    <?php include "filebottom.php"; ?>


  </div>

</body>
</html>