<!DOCTYPE html>
<html>
<head>
	<title>Home</title>

<?php include "filehead.php"; ?>


</head>

<body class="grey darken-4">

  <div class="container white background">

	<?php include "header.php"; ?>

  <div class="col s12 breadalign">
    <a href="index.php" class="breadcrumb">Pagrindinis</a>
    <a href="about.php" class="breadcrumb">Apie</a>
    <a href="vytasinfo.php" class="breadcrumb">Vycka</a>
  </div>

    <h3 class="center-align">Vytautas Skywalker</h3>
    <h6 class="grey-text text-darken-2 center-align quote">Moto</h6>
  
    <div class="row">

      <div class="col s12 m12 l12 ">

        <p class="center-align">Kadaise Naskos dykuma neva ir buvusi tarpgalaktinis kosmodromas. Šiuolaikiniai sveiko proto lakūnai vienu balsu tvirtina, kad tokiais primargintais, ilgais pakilimo ir nusileidimo takais galėtų naudotis nebent pamišėliai. Gal tokie ir buvo kitų planetų ateiviai?</p>

          <div class="row center-align">
          <div class="col s12 m12 l12">
          <img src="https://picsum.photos/id/1079/250/150" class="card personimg">
          <img src="https://picsum.photos/id/331/250/150" class="card personimg">
          <img src="https://picsum.photos/id/117/250/150" class="card personimg">
        </div>
         </div>
      </div>

    </div>


  <?php include "footer.php"; ?>

  <?php include "filebottom.php"; ?>


</div>

</body>
</html>